package main

import (
	"context"
	"fmt"
	"worker/internal/manager"
	"worker/internal/parser"
	"worker/internal/storage"
)

func main() {
	strategyFactory := parser.NewStrategyFactory()
	fileParser := parser.NewUniversalParser(strategyFactory)
	fileStorage := storage.NewStorageHttpClient(&storage.HttpClientConfig{})
	jobManager := manager.NewRMQClient(&manager.RMQClientConfig{})

	errCh := jobManager.Subscribe(func(ctx context.Context, j *manager.Job) (*parser.ParseResult, error) {
		file := j.File
		binary, err := fileStorage.Get(ctx, file.ID)
		if err != nil {
			return nil, err
		}

		result, err := fileParser.Do(ctx, file.Extension, binary)
		if err != nil {
			return nil, err
		}

		return result, nil
	})

	for err := range errCh {
		fmt.Println(err)
	}
}

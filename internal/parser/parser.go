package parser

import (
	"context"
	"time"
)

type Parser interface {
	Do(ctx context.Context, extension string, binary []byte) (*ParseResult, error)
}

type warning interface {
}

type Value struct {
	Filters map[string]string
	Filler  []string
	Value   float64
}

type ParseResult struct {
	Values   []*Value
	Warnings []string
	Errors   []string
}

type Config struct {
	Timeout time.Duration
}

type UniversalParser struct {
	strategies StrategyCreator
	config     Config
}

func (u *UniversalParser) Do(ctx context.Context, extension string, binary []byte) (*ParseResult, error) {
	strategy, err := u.strategies.Create(extension)
	if err != nil {
		return nil, err
	}
	ctxTimeout, cancel := context.WithTimeout(ctx, u.config.Timeout)
	defer cancel()
	return strategy.Parse(ctxTimeout, binary)
}

func NewUniversalParser(creator StrategyCreator) *UniversalParser {
	return &UniversalParser{strategies: creator}
}

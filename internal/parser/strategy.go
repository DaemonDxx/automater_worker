package parser

import (
	"context"
	"errors"
	"sync"
	"worker/internal/parser/strategy"
)

const (
	XLSX = "xlsx"
	XLSM = "xlsm"
)

var NotSupportErr = errors.New("file not supported")

type Strategy interface {
	Parse(ctx context.Context, binary []byte) (*ParseResult, error)
}

type StrategyCreator interface {
	Create(extension string) (Strategy, error)
}

type StrategyFactory struct {
	cache map[string]Strategy
	m     sync.RWMutex
}

func (sf *StrategyFactory) get(fileType string) (Strategy, bool) {
	sf.m.RLock()
	defer sf.m.RUnlock()
	s, ok := sf.cache[fileType]
	return s, ok
}

func (sf *StrategyFactory) set(fileTypes []string, strategy Strategy) {
	sf.m.Lock()
	defer sf.m.Unlock()
	for _, filetype := range fileTypes {
		sf.cache[filetype] = strategy
	}
}

func (sf *StrategyFactory) Create(extension string) (Strategy, error) {
	switch extension {
	case XLSM, XLSX:
		if s, ok := sf.get(extension); ok {
			return s, nil
		} else {
			s := strategy.NewExcelStrategy()
			types := []string{XLSM, XLSX}
			sf.set(types, s)
			return s, nil
		}
	default:
		return nil, NotSupportErr
	}
}

func NewStrategyFactory() *StrategyFactory {
	return &StrategyFactory{
		cache: make(map[string]Strategy),
	}
}

package strategy

import (
	"context"
	"worker/internal/parser"
)

type ExcelStrategy struct{}

func (e ExcelStrategy) Parse(ctx context.Context, binary []byte) (*parser.ParseResult, error) {
	panic("implement me")
}

func NewExcelStrategy() *ExcelStrategy {
	return &ExcelStrategy{}
}

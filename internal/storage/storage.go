package storage

import (
	"context"
	"errors"
)

var FileNotFoundErr = errors.New("file not found")

type Client interface {
	Get(ctx context.Context, id int) ([]byte, error)
}

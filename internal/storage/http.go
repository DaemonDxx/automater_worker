package storage

import "context"

type HttpClientConfig struct {
}

type HttpClient struct {
}

func (h HttpClient) Get(ctx context.Context, id int) ([]byte, error) {
	panic("implement me")
}

func NewStorageHttpClient(config *HttpClientConfig) *HttpClient {
	return &HttpClient{}
}

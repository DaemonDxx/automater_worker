package manager

type RMQClientConfig struct {
}

type RMQClient struct {
}

func (R RMQClient) Subscribe(handler HandleFunc) <-chan error {
	panic("implement me")
}

func (R RMQClient) Unsubscribe() error {
	panic("implement me")
}

func NewRMQClient(config *RMQClientConfig) *RMQClient {
	return nil
}

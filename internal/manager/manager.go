package manager

import (
	"context"
	"worker/internal/parser"
)

type FileType string

const (
	TEMPLATE FileType = "TEMPLATE"
	SOURCE   FileType = "SOURCE"
)

type File struct {
	ID        int
	Extension string
	Type      FileType
}

type Job struct {
	ID   int
	File File
}

type HandleFunc func(ctx context.Context, j *Job) (*parser.ParseResult, error)

type Client interface {
	Subscribe(handler HandleFunc) <-chan error
	Unsubscribe() error
}
